#include <cmath>
#include <iostream>
#include <vector>

using pair_table = std::vector<std::vector<std::pair<int, int>>>;

pair_table PreprocessSparse(
        std::vector<int> &a, int n) {
  pair_table sp(n, std::vector<std::pair<int, int>>((int) log2(n) + 1, std::make_pair(0, 0)));
  for (int i = 0; i < n; ++i) {
    sp[i][0] = std::make_pair(a[i], i);
  }
  for (int j = 1; (1 << j) <= n; ++j) {
    for (int i = 0; (i + (1 << j) - 1) < n; ++i) {
      sp[i][j] = std::min(sp[i][j - 1], sp[i + (1 << (j - 1))][j - 1]);
    }
  }
  return sp;
}

int Query(pair_table &sp, int l, int r) {
  int j = (int) log2(r - l + 1);
  int l1, r1, l2, r2, min1, min2, j1, j2;
  int index;
  if (sp[l][j].first <= sp[r - (1 << j) + 1][j].first) {
    index = sp[l][j].second;
  } else {
    index = sp[r - (1 << j) + 1][j].second;
  }
  l1 = l;
  r1 = index - 1;
  l2 = index + 1;
  r2 = r;
  j1 = (int) log2(r1 - l1 + 1);
  j2 = (int) log2(r2 - l2 + 1);
  if (j1 < 0) {
    min1 = INT_MAX;
  } else {
    min1 = std::min(sp[l1][j1].first, sp[r1 - (1 << j1) + 1][j1].first);
  }
  if (j2 < 0) {
    min2 = INT_MAX;
  } else {
    min2 = std::min(sp[l2][j2].first, sp[r2 - (1 << j2) + 1][j2].first);
  }
  return std::min(min1, min2);
}

int main() {
  int n, m, num, x, y;
  std::vector<int> v;
  std::cin >> n >> m;
  for (int i = 0; i < n; ++i) {
    std::cin >> num;
    v.emplace_back(num);
  }
  pair_table sp = PreprocessSparse(v, n);
  for (int i = 0; i < m; ++i) {
    std::cin >> x >> y;
    std::cout << Query(sp, x - 1, y - 1) << "\n";
  }
  return 0;
}
