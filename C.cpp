#include <cmath>
#include <iostream>
#include <vector>

using PairTable = std::vector<std::pair<int, int>>;

class SegmentTree {
 public:
  PairTable tree;
  PairTable vec;
  SegmentTree(PairTable& vec, int s) {
    this->vec = vec;
    for (int j = 0; j < 4 * s; ++j) {
      tree.emplace_back(0, 0);
    }
  }

  void Update(int pos, int val, int v, int tl, int tr) {
    if (pos <= tl && tr <= pos) {
      tree[v].first = val;
      return;
    }
    if (tr < pos || pos < tl) {
      return;
    }
    int tm = (tl + tr) / 2;
    Update(pos, val, v * 2, tl, tm);
    Update(pos, val, v * 2 + 1, tm + 1, tr);
    tree[v].first = std::max(tree[v * 2].first, tree[v * 2 + 1].first);
    tree[v].second = std::max(tree[v * 2].second, tree[v * 2 + 1].second);
  }

  int Find(int pos, int val, int v, int tl, int tr) {
    if (tree[v].first < val || tree[v].second < pos) {
      return -1;
    }
    if (tl == tr && tree[v].second >= pos) {
      return tree[v].second + 1;
    }
    int tm = (tl + tr) / 2;
    int ans = Find(pos, val, 2 * v, tl, tm);
    if (ans == -1) {
      return Find(pos, val, 2 * v + 1, tm + 1, tr);
    }
    return ans;
  }

  void Build(int v, int tl, int tr) {
    if (tl == tr) {
      tree[v] = vec[tl];
    } else {
      int tm = (tl + tr) / 2;
      Build(v * 2, tl, tm);
      Build(v * 2 + 1, tm + 1, tr);
      tree[v].first = std::max(tree[v * 2].first, tree[v * 2 + 1].first);
      tree[v].second = std::max(tree[v * 2].second, tree[v * 2 + 1].second);
    }
  }
};

int main() {
  int request, x, y, m, pos, n;
  PairTable vec;
  std::cin >> n >> m;
  for (int j = 0; j < n; ++j) {
    std::cin >> y;
    vec.emplace_back(y, j);
  }
  int st = 1 << ((int)log2(n) + 1);
  for (int i = 0; i < (n - st); ++i) {
    vec.emplace_back(0, 0);
  }
  int s = vec.size();
  SegmentTree* t = new SegmentTree(vec, s);
  t->Build(1, 0, n - 1);
  for (int i = 0; i < m; ++i) {
    std::cin >> request >> pos >> x;
    if (request == 0) {
      t->Update(pos - 1, x, 1, 0, s - 1);
    } else {
      std::cout << t->Find(pos - 1, x, 1, 0, s - 1) << "\n";
    }
  }
  delete t;
}
